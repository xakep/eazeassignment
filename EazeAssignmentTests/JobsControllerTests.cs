﻿namespace EazeAssignmentTests
{
    using EazeAssignment.Controllers;
    using EazeAssignment.Models;
    using EazeAssignment.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System.Threading.Tasks;
    using System.Web.Http.Results;

    [TestClass]
    public class JobsControllerTests
    {
        [TestMethod]
        public async Task BadRequestExpectedIfNoUrlProvided()
        {
            // Arrange
            var controller = new JobsController(null);

            // Act
            var res = await controller.Post(null);

            // Assert
            Assert.IsTrue(res.GetType() == typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public async Task BadRequestExpectedIfEmptyStringProvidedAsUrl()
        {
            // Arrange
            var controller = new JobsController(null);

            // Act
            var res = await controller.Post(string.Empty);

            // Assert
            Assert.IsTrue(res.GetType() == typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public async Task PostNewJobShouldBeCalled()
        {
            // Arrange
            var service = Mock.Of<IJobsService>();
            var controller = new JobsController(service);
            var url = "url";
            Mock.Get(service)
                .Setup(m => m.PostNewJob(url))
                .Returns(new JobStatus(Status.Finished, 0));

            // Act
            var res = await controller.Post(url);

            // Assert
            Mock.Get(service).Verify(m => m.PostNewJob(url), Times.Once);
        }

        [TestMethod]
        public async Task BadRequestExpectedIfJobIsAlreadyExists()
        {
            // Arrange
            var service = Mock.Of<IJobsService>();
            var controller = new JobsController(service);
            var url = "url";
            Mock.Get(service)
                .Setup(m => m.PostNewJob(url))
                .Returns(new JobStatus(Status.Finished, 0));

            // Act
            var res = await controller.Post(url);

            // Assert
            Assert.IsTrue(res.GetType() == typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public async Task OkExpectedIfJobSuccessfullyCreated()
        {
            // Arrange
            var service = Mock.Of<IJobsService>();
            var controller = new JobsController(service);
            var url = "url";
            Mock.Get(service)
                .Setup(m => m.PostNewJob(url))
                .Returns(new JobStatus(Status.Created, 0));

            // Act
            var res = await controller.Post(url);

            // Assert
            Assert.IsTrue(res.GetType() == typeof(OkNegotiatedContentResult<JobStatus>));
            Assert.AreEqual(
                Status.Created,
                ((res as OkNegotiatedContentResult<JobStatus>).Content as JobStatus).Status);
        }

        // Sorry, no more tests
        // I could make the 100% coverage, but it will take much time, I don't have it
    }
}
