﻿namespace EazeAssignment.Services
{
    using System.Threading.Tasks;

    public interface IWebScraper
    {
        Task<string> Scrape(string url);
    }
}
