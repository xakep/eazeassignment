﻿namespace EazeAssignment.Services
{
    using Models;

    public interface IJobsQueue
    {
        void AddJob(Job job);

        void ProcessJobs();
    }
}
