﻿namespace EazeAssignment.Services
{
    using Models;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;

    public class JobsProcessor : IJobsProcessor
    {
        private readonly IJobIdGenerator jobIdGenerator;
        private readonly IJobsQueue jobsQueue;
        private readonly int threadsCount = 10;

        // match url to job id
        private readonly ConcurrentDictionary<int, string> jobIds;

        // keeps all created jobs
        private readonly ConcurrentDictionary<string, Job> jobs;

        public JobsProcessor(IJobIdGenerator jobIdGenerator, IJobsQueue jobsQueue)
        {
            this.jobIdGenerator = jobIdGenerator;
            this.jobsQueue = jobsQueue;

            this.jobs = new ConcurrentDictionary<string, Job>();
            this.jobIds = new ConcurrentDictionary<int, string>();

            for (var i = 0; i < threadsCount; i++)
            {
                Task.Run(() => this.jobsQueue.ProcessJobs());
            }
        }

        public Job PostNewJob(string url)
        {
            var newId = this.jobIdGenerator.GetNextJobId();

            // adds new jobs or gets existing one
            var job = this.jobs.AddOrUpdate(
                url,
                new Job(url, newId),
                (key, value) => value);

            this.jobIds.GetOrAdd(job.Id, url);
            if (newId == job.Id)
            {
                // this is a new job, queue it and set status
                this.jobsQueue.AddJob(job);
                job.SetStatus(Status.Queued);
            }

            return job;
        }

        public JobResult GetJobResult(int id)
        {
            string val = null;
            var exists = this.jobIds.TryGetValue(id, out val);
            if (!exists)
            {
                return null;
            }

            Job job = null;
            jobs.TryGetValue(val, out job);

            return new JobResult(job.Status, job.Result);
        }
    }
}