﻿namespace EazeAssignment.Services
{
    using Models;

    public class JobsService : IJobsService
    {
        private readonly IJobsProcessor jobsProcessor;

        public JobsService(IJobsProcessor jobsProcessor)
        {
            this.jobsProcessor = jobsProcessor;
        }

        public JobResult GetJobById(int id)
        {
            return this.jobsProcessor.GetJobResult(id);
        }

        public JobStatus PostNewJob(string url)
        {
            var job = this.jobsProcessor.PostNewJob(url);
            return new JobStatus(job.Status, job.Id);
        }
    }
}