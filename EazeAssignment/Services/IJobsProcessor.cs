﻿namespace EazeAssignment.Services
{
    using Models;

    public interface IJobsProcessor
    {
        Job PostNewJob(string url);

        JobResult GetJobResult(int id);
    }
}
