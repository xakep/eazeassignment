﻿namespace EazeAssignment.Services
{
    using Models;

    public interface IJobsService
    {
        JobStatus PostNewJob(string url);

        JobResult GetJobById(int id);
    }
}
