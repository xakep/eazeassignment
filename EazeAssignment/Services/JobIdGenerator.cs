﻿namespace EazeAssignment.Services
{
    using System.Threading;

    public class JobIdGenerator : IJobIdGenerator
    {
        private static int count = 0;

        public int GetNextJobId()
        {
            Interlocked.Increment(ref count);

            return count;
        }
    }
}