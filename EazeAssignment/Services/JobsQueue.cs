﻿namespace EazeAssignment.Services
{
    using Models;
    using System;
    using System.Collections.Concurrent;
    using System.Threading;

    public class JobsQueue : IJobsQueue
    {
        private readonly IWebScraper webScraper;
        private readonly BlockingCollection<Job> queue;

        public JobsQueue(IWebScraper webScraper)
        {
            this.webScraper = webScraper;
            this.queue = new BlockingCollection<Job>(new ConcurrentQueue<Job>());
        }

        public void AddJob(Job job)
        {
            this.queue.Add(job);
        }

        public async void ProcessJobs()
        {
            while (true)
            {
                try
                {
                    var job = this.queue.Take();
                    job.SetStatus(Status.InProgress);

                    job.SetResult(await this.webScraper.Scrape(job.Url));
                    job.SetStatus(Status.Finished);
                }
                catch (InvalidOperationException)
                {
                    //
                }
            }
        }
    }
}