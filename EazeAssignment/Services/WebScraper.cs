﻿namespace EazeAssignment.Services
{
    using System;
    using System.Threading.Tasks;

    public class WebScraper : IWebScraper
    {
        public async Task<string> Scrape(string url)
        {
            var rand = new Random();
            var delay = rand.Next(2, 15);
            await Task.Delay(delay * 1000);

            return string.Format("Finished in {0} sec", delay);
        }
    }
}