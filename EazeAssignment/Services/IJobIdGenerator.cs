﻿namespace EazeAssignment.Services
{
    public interface IJobIdGenerator
    {
        int GetNextJobId();
    }
}
