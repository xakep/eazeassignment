﻿namespace EazeAssignment.Models
{
    public class JobStatus
    {
        public Status Status { get; private set; }

        public int Id { get; private set; }

        public JobStatus(Status status, int id)
        {
            this.Status = status;
            this.Id = id;
        }
    }
}