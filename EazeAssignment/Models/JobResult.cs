﻿namespace EazeAssignment.Models
{
    using Newtonsoft.Json;

    public class JobResult
    {
        public Status Status { get; private set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Result { get; private set; }

        public JobResult(Status status, string result = null)
        {
            this.Status = status;
            this.Result = result;
        }
    }
}