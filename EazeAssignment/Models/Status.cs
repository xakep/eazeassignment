﻿namespace EazeAssignment.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Status
    {
        Created,
        Queued,
        InProgress,
        Finished
    }
}