﻿namespace EazeAssignment.Models
{
    public class Job
    {
        public string Url { get; private set; }

        public int Id { get; private set; }

        public Status Status { get; private set; }

        public string Result { get; private set; }

        public Job(string url, int id)
        {
            this.Url = url;
            this.Id = id;
            this.Status = Status.Created;
        }

        public void SetStatus(Status status)
        {
            this.Status = status;
        }

        public void SetResult(string result)
        {
            this.Result = result;
        }
    }
}