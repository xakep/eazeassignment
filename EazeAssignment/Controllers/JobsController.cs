﻿namespace EazeAssignment.Controllers
{
    using Models;
    using Services;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class JobsController : ApiController
    {
        private readonly IJobsService jobsService;

        public JobsController(IJobsService jobsService)
        {
            this.jobsService = jobsService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetById(int id = 0)
        {
            var result = this.jobsService.GetJobById(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return BadRequest("Required parameter: url");
            }

            var existingJob = this.jobsService.PostNewJob(url);
            if (existingJob.Status != Status.Created)
            {
                var message = string.Format("The job with id {1} is {0}", existingJob.Status, existingJob.Id);
                return BadRequest(message);
            }

            // create new job
            return Ok(existingJob);
        }

        // improvements:
        // 1. cancel the job
        // 2. keep results 30 mins for example, then clean it
        // I would use nosql db for this such as couchbase (can clean automatically after some time)
    }
}
