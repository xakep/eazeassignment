﻿namespace EazeAssignment
{
    using Services;
    using System.Web.Http;
    using WebApi.StructureMap;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.UseStructureMap(x =>
            {
                x.ForSingletonOf<IJobsService>().Use<JobsService>();
                x.ForSingletonOf<IJobsProcessor>().Use<JobsProcessor>();
                x.ForSingletonOf<IJobIdGenerator>().Use<JobIdGenerator>();
                x.ForSingletonOf<IJobsQueue>().Use<JobsQueue>();
                x.ForSingletonOf<IWebScraper>().Use<WebScraper>();
            });

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
